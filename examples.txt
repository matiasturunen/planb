---- cd ----
cd
cd ..
cd /bin

---- sleep ----
sleep 10
sleep 10 &

---- ls ----
ls
ls -la
ls -l -a

---- cat ----
cat file
cat -b file

---- etc ----
less
tail
head
wc
uname
jne.

---- redirect ----
wc < file
wc < file1 < file2 < file3
ls > file
cat file > other_file


---- pipe ----
ls | wc
ls | wc | grep 1

Käytä välilyöntäjä jos ei toimi.


