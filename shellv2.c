#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>

#define MAXLEN 128

int callCount = 0;
int background = 0;
char buffer[1024];
char line[1024];
char * cmdArgs[MAXLEN];
char * redirCmds[MAXLEN];
pid_t pid;

void split(char* cmd);
void waitAll(int n);
void cmdToFile(char * cmd, char * fileFrom, char* fileTo);
char * removeLeadingWhite(char* str);
char * trimSpaces(char * s);
int runcmd(int inputsrc, int isfirst, int islast);
int run(char* cmd, int inputsrc, int isfirst, int islast);
int redirRightLeft(char * redirFrom, int inputsrc, int isfirst, int islast);
int redir(char * filename, int inputsrc, int isfirst);
int writeToFile(char * filename, int inputsrc);

int main(void) {
    int inputsrc;
    int isfirst;

    while(1) {
        background = 0;
        // print prompt and flush
        fflush(NULL);
        printf("%s$> ", getcwd(buffer, 1024));
        fflush(NULL);

        // read command or exit
        if (fgets(line, 1024, stdin) == NULL) return 0;

        if (line[strlen(line)-2] == '&') {
            line[strlen(line)-2]=0;
            background = 1;
        }

        inputsrc = 0;
        isfirst = 1;

        char * greater;
        char * lesser;
        char tLine[1024];

        strcpy(tLine, line);

        // find all possible pipes, ie "|" chars.
        char * cmd = line;
        char * next = strchr(cmd, '|');
        while (next != NULL) {
            // 'remove' first char (it's a '|')
            next[0] = '\0';

            inputsrc = redirRightLeft(cmd, inputsrc, isfirst, 0);

            cmd = next+1;
            next = strchr(cmd, '|');
            isfirst = 0;
        }

        greater = strtok(cmd, ">");
        greater = strtok(NULL, ">");
        if (greater != NULL) {
                lesser = strchr(tLine, '<');
            if (lesser != NULL) {

                char * fileFrom = strtok(cmd, "<");
                fileFrom = strtok(NULL, "<");

                cmdToFile(cmd, fileFrom, greater);

            } else {
                inputsrc = redirRightLeft(cmd, inputsrc, isfirst, 0);
                writeToFile(greater, inputsrc);
            }
        } else {
            inputsrc = redirRightLeft(cmd, inputsrc, isfirst, 1);
        }

        if (!background) {
            waitAll(callCount);
            callCount = 0;
        }
    }

    return 0;
}

int redir(char * filename, int inputsrc, int isfirst) {
    int fd;

    // remove newline, if exists
    if (filename[strlen(filename)-1] == '\n') {
        filename[strlen(filename)-1] = '\0';
    }

    // write
    if (!isfirst) {
        writeToFile(filename, inputsrc);
    }
    
    // read
    if (access(filename, F_OK) == 0) {
        fd = open(filename, O_RDONLY);
        dup2(fd, STDIN_FILENO);
        return fd;
    } else {
        //printf("NO FILE\n");
    }

    return STDIN_FILENO;
}

void cmdToFile(char * cmd, char * fileFrom, char* fileTo) {
    if (!fork()) {
        int fd;
        
        fileFrom = trimSpaces(fileFrom);
        fileTo = trimSpaces(fileTo);

        split(cmd);
        callCount++;
        //printf("%s\n", cmdArgs[0]);
        printf("\n");
        //printf("cmd: %s, from %s, to %s\n", cmd, fileFrom, fileTo);

        fd = open(fileFrom, O_RDONLY);
        dup2(fd, STDIN_FILENO);
        close(fd);

        fd = open(fileTo, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        dup2(fd, STDOUT_FILENO);
        close(fd);

        execvp(cmdArgs[0], cmdArgs);
    } else {
        wait(NULL);
    }

}

int writeToFile(char * filename, int inputsrc) {
    // remove newline, if exists
    if (filename[strlen(filename)-1] == '\n') {
        filename[strlen(filename)-1] = '\0';
    }

    filename = trimSpaces(filename);
    int target = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    ssize_t bytes;
    while((bytes = read(inputsrc, &buffer, 1024))) {
        write(target, buffer, bytes);
    }
    close(target);

    return 0;

}

char * trimSpaces(char * s) {
    char * end;
    s = removeLeadingWhite(s);
    end = s + strlen(s) - 1;
    while(end > s && isspace(*end)) end--;
    *(end+1) = 0;
    return s;
}

int redirRightLeft(char * redirFrom, int inputsrc, int isfirstpipe, int islastpipe) {
    char *redirTo;
    char * temp[MAXLEN];
    int j = 0, i = 0, isfirstRedir = 1;

    // split and reverse commands
    redirTo = redirFrom;
    while ( (temp[i] = strtok(redirTo, "<")) != NULL ) {
        i++;
        redirTo = NULL;
    }

    while (temp[i-1] != NULL) {
        redirCmds[j] = temp[i-1];
        i--; j++;
    }
    redirCmds[j] = NULL;


    // run commands
    i = 0;
    while (redirCmds[i] != NULL) {
        redirCmds[i] = removeLeadingWhite(redirCmds[i]);

        // check if there is next command
        if (redirCmds[i+1] != NULL) {
            inputsrc = redir(redirCmds[i], 0, isfirstRedir);
            isfirstRedir = 0;
        } else {
            return run(redirCmds[i], inputsrc, isfirstpipe, islastpipe);
        }
        
        i++;
    }
    return STDOUT_FILENO;
}

int run(char* cmd, int inputsrc, int isfirst, int islast) {
    // split to get args
    split(cmd);

    if(cmdArgs[0] != NULL) {
        // exit
        if (strcmp(cmdArgs[0], "exit") == 0) {
            exit(0);
        }

        // cd
        if (strcmp(cmdArgs[0], "cd") == 0) {
            chdir((cmdArgs[1]==NULL) ? getenv("HOME") : cmdArgs[1]);
            return 0;
        }
        
        callCount++;
        return runcmd(inputsrc, isfirst, islast);

    }
    return 0;
}

int runcmd(int inputsrc, int isfirst, int islast) {
    int pipes[2];
    // pipes[0] = read-end
    // pipes[1] = write-end

    // make pipe
    pipe(pipes);

    // fork
    pid = fork();

    if (pid == 0) {
        if (isfirst == 1 && islast == 0 && inputsrc == 0) {
            // first command in pipeline
            dup2(pipes[1], STDOUT_FILENO);
        } else if (isfirst == 0 && islast == 0 && inputsrc != 0) {
            // command in middle of pipeline
            dup2(inputsrc, STDIN_FILENO);
            dup2(pipes[1], STDOUT_FILENO);
        } else {
            // last command in pipeline
            dup2(inputsrc, STDIN_FILENO);
        }

        execvp(cmdArgs[0], cmdArgs);

    }

    if (inputsrc != 0) close(inputsrc);

    // close write end
    close(pipes[1]);

    // close read if last cmd
    if (islast) close(pipes[0]);

    return pipes[0];
}

// waits n child processes to finish
void waitAll(int n) {
    for(int i=0; i<n; i++) {
        wait(NULL);
    }
}

// remove extra whitespace from beginning
char * removeLeadingWhite(char* str) {
    while(isspace(*str)) ++str;
    return str;
}

void split(char* cmd) {
    int i = 0;
    char * next;

    // split at space
    cmd = removeLeadingWhite(cmd);
    next = strchr(cmd, ' ');

    while(next != NULL) {
        cmdArgs[i] = cmd;
        next[0] = '\0';
        cmd = removeLeadingWhite(next + 1); // let command be what is left after delimiter
        next = strchr(cmd, ' ');
        i++;
    }

    // take care on the last cmd
    if (cmd[0] != '\0') {
        cmdArgs[i] = cmd;
        next = strchr(cmd, '\n');
        next[0] = '\0';
        i++;
    }

    cmdArgs[i] = NULL;  // make sure that we have some point to stop in the args list

}
